# README

Create project:

```
oc new-app -f ./openshift/app-springboot-config-server-template.yaml
```


```
oc start-build app-config-server-binary --from-dir=./docker/config-server --wait=true
```